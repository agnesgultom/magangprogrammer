<?php

namespace Tests\Feature;

use App\Model\Kalkulator;
use Tests\TestCase;

class KalkulatorTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    /** @test */
    public function can_calculate_result()
    {
        $addition = new Kalkulator("10+5");
        $this->assertEquals(15,$addition->perhitungan(),"Benar");
    }
    /** @test */
    public function can_check_character()
    {
        $addition = new Kalkulator("10+a");
        $this->assertEquals("Masukan merupakan huruf atau karakter",$addition->perhitungan(),"Benar");
    }
    /** @test */
    public function can_check_divide_zero()
    {
        $addition = new Kalkulator("10/0");
        $this->assertEquals("Tidak dapat dibagi",$addition->perhitungan(),"Benar");
    }
}
