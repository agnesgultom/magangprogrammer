<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GanjilGenap extends Model
{
    public $inputan1;
    public $inputan2;
    public $hasil = array();
    function  __construct($inputan1, $inputan2, $attributes=array())
    {
        parent::__construct($attributes);
        $this->inputan1=$inputan1;
        $this->inputan2=$inputan2;
    }

    function cekGenapGanjil(){
        for($bil = $this->inputan1; $bil <= $this->inputan2; $bil++){
            if($bil%2==0){
                $hasil[$bil] =  "Bilangan genap";
            } else {
                $hasil[$bil] = "Bilangan ganjil";
            }
        }
        return $hasil;
    }
}
