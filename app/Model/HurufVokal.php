<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HurufVokal extends Model
{   public $inputan;

    function  __construct($inputan, $attributes=array())
    {
        parent::__construct($attributes);
        $this->inputan=$inputan;
    }

    function hitungVocal(){
        $xss =htmlspecialchars($this->inputan,ENT_QUOTES);
        $arr = str_split($xss);
        $vocal = ['a','i','u','e','o'];
        $found = array_intersect($vocal, $arr);
        $count = count($found);
        $final = implode(" ", $found);
        return "$this->inputan = " .$count." yaitu " .$final;
    }
}
