<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Kalkulator extends Model
{
    public $inputan;
    public $operator;

    function  __construct($inputan, $attributes=array())
    {
        parent::__construct($attributes);
        $this->inputan=$inputan;
    }

    function perhitungan(){
        $operator = array();
        $inputan1 = str_replace(' ','', $this->inputan);
        for($i=0; $i<=strlen($inputan1)-1;$i++){
            if(($inputan1[$i]=="+") || ($inputan1[$i]=="-") || ($inputan1[$i]=="*") || ($inputan1[$i]=="/")){
                $operator[]=$inputan1[$i];
            }
        }

        $inputan1 = str_replace("+", " ", $inputan1);
        $inputan1 = str_replace("-", " ", $inputan1);
        $inputan1 = str_replace("*", " ", $inputan1);
        $inputan1 = str_replace("/", " ", $inputan1);

        $operand = explode(" ", $inputan1);

        $this->inputan = $operand[0];
        $cek = Arr::accessible($this->inputan);
        try {
            if ($cek == 1){
                throw new \Exception("Huruf atau karakter");
            }
            for($i=0; $i<=count($operator)-1; $i++){
                if($operator[$i] == "+") $this->inputan = $this->inputan + $operand[$i+1];
                else if ($operator[$i] == "-") $this->inputan = $this->inputan - $operand[$i+1];
                else if ($operator[$i] == "*") $this->inputan = $this->inputan * $operand[$i+1];
                else if ($operator[$i] == "/")
                {
                    try {
                        if($operand[$i+1] == 0){
                            throw new \Exception("Pembagian nol");
                        }
                        $this->inputan = $this->inputan / $operand[$i+1];
                    } catch (\Exception $e) {
                        return $this->inputan = "Tidak dapat dibagi";
                    }
                }
            }
        }catch (\Exception $e){
            return $this->inputan ="Masukan merupakan huruf atau karakter";
        }
        return $this->inputan;
    }
}
