<?php

namespace App\Http\Controllers;

use App\Model\HurufVokal;
use Illuminate\Http\Request;

class HurufVokalController extends Controller
{
    public function hurufvokalFunc(Request $request){
        $inputan = $request->input('inputan');
        $cekhurufvokal = new HurufVokal($inputan);
        $result = $cekhurufvokal->hitungVocal();

        return redirect('/hitunghurufvokal')->with('info', $result);
    }
}
