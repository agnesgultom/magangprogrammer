<?php

namespace App\Http\Controllers;

use App\Model\GanjilGenap;
use Illuminate\Http\Request;

class GanjilGenapController extends Controller
{
    public function ganjilgenapFunc(Request $request){
        $inputan1 = $request->input('inputan1');
        $inputan2 = $request->input('inputan2');
        $cekgenapganjil = new GanjilGenap($inputan1, $inputan2);
        $result = $cekgenapganjil->cekGenapGanjil();
        return view('/view',['result'=>$result]);
    }
}
