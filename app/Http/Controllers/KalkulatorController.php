<?php

namespace App\Http\Controllers;
use App\Model\Kalkulator;
use Illuminate\Http\Request;

class   KalkulatorController extends Controller
{
    public function kalkulatorFunc(Request $request)
    {
        $inputan = $request->input('inputan');
        $kalkulator = new Kalkulator($inputan);
        $result = $kalkulator->perhitungan();

        return redirect('/kalkulator')->with('info', 'Hasilnya adalah : ' . $result);
    }
}
