<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('kalkulator');
});
Route::get('/kalkulator', function () {
    return view('kalkulator');
});
Route::post('/kalkulator', 'KalkulatorController@kalkulatorFunc');

Route::get('/ganjilgenap', function () {
    return view('ganjilgenap');
});
Route::post('/ganjilgenap', 'GanjilGenapController@ganjilgenapFunc');

Route::get('/hitunghurufvokal', function(){
    return view('hitunghurufvokal');
});

Route::post('/hitunghurufvokal', 'HurufVokalController@hurufvokalFunc');
