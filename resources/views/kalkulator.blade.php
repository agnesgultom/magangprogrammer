@extends('welcome')

@section('title', 'Kalkulator')

@section('container')
    <h1>Kalkultor</h1>
<form action="/kalkulator" method="POST">
    @csrf
    <div class="container" style="margin-top: 30px">
        <div class="row justify-conteYnt-center">
            <div class="col-md-3">
                <input type="text" name="inputan" class="form-control">
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-info">Hasil</button>
            </div>
        </div>
    </div>
</form>
<div class="row">
    <div class="col-md=2">

    </div>
    <div class="col-md=3">
        @if(session('info'))
            <div class="alert alert-info" style="text-align: center; margin-top: 15px">
                {{ session('info') }}
            </div>
        @endif
    </div>
    <div class="col-md=2">

    </div>
</div>
@endsection
