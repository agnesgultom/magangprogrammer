@extends('welcome')

@section('title', 'Hitung Huruf Vokal')

@section('container')
<h1>Hitung Huruf Vokal</h1>
<form action="/hitunghurufvokal" method="POST">
    @csrf
    <div class="container" style="margin-top: 30px">
        <div class="row">
            <div class="col-md-3">
                <label for="inputan" class="form-label">Masukkan kata/kalimat</label>
                <textarea type="text" name="inputan" class="form-control" placeholder="Masukkan kata/kalimat...."></textarea>
            </div>
        </div>
        <div class="row>">
            <div class="col-md-3" style="margin-top: 10px;">
                <button type="submit" class="btn btn-info">Hasil</button>
            </div>
        </div>
    </div>
</form>

<div class="col-md=3">
    @if(session('info'))
        <div class="alert alert-info" style="text-align: center; margin-top: 15px">
            {{ session('info') }}
        </div>
    @endif
</div>
@endsection
