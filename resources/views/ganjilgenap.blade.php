@extends('welcome')

@section('title', 'Genap Ganjil')

@section('container')
<h1>Ganjil Genap</h1>
<form action="/ganjilgenap" method="POST">
    @csrf
    <div class="container" style="margin-top: 30px">
        <div class="row">
            <div class="col-md-3">
                <label for="inputan1" class="form-label">Angka Pertama</label>
                <input type="number" name="inputan1" class="form-control" placeholder="Masukkan angka pertama">
            </div>
            <div class="col-md-3">
                <label for="inputan2" class="form-label">Angka Kedua</label>
                <input type="number" name="inputan2" class="form-control" placeholder="Masukkan angka kedua">
            </div>
        </div>
        <div class="row>">
            <div class="col-md-3" style="margin-top: 10px;">
                <button type="submit" class="btn btn-info">Hasil</button>
            </div>
        </div>
    </div>
</form>
@endsection
