@extends('welcome')

@section('title', 'Hasil Genap Ganjil')

@section('container')
<div class="col-md=3">
        <table class="table" style="width: 75%">
            <thead class="thead-dark">
            <tr>
                <td>Angka</td>
                <td>Hasil</td>
            </tr>
            </thead>
            @foreach ($result as $hasil => $hsl)
            <tr><td>{{$hasil++}}</td>
                <td> {{$hsl}}</td>
            </tr>
            @endforeach
        </table>
</div>
@endsection
